const http = require('http');

module.exports = (config,app) => {
  app.get('/api/get/all', (req,res) => {
    http.get({
          host: config.API_HOST, 
          port: config.API_PORT, 
          path: '/api/get/all'
      }, (response) => {
      let reBody = '';
      response.on('data', (d) => {
        reBody += d;
      });
      response.on('end', () => {
        let parsed = JSON.parse(reBody);
        res.status(200).json(parsed);
      });
    });
  });

  app.post('/api/get/all', (req,res) => {
    res.status(200).json([{title:'hello world',text:'some text not sure how many char should be en'}]);
  });

  app.post('/api/post', (req,res) => {
    let postIt = http.request({
        host: config.API_HOST, 
        port: config.API_PORT, 
        path: '/api/post',
        method: 'POST',
        headers: {'Content-Type':'application/json'},
      }, (response) => {
        let reBody = '';
        response.setEncoding('utf8');
        response.on('data', (d) => {
          reBody += d;
        });
        response.on('end', () => {
          let parsed = JSON.parse(reBody);
          res.status(200).json(parsed);
        });
    });
    postIt.write(JSON.stringify(req.body));
    postIt.end();
  });
};