const config = {};

config.API_HOST = process.env.API_HOST||"localhost";
config.API_PORT = process.env.API_PORT||8074;

config.HOST = process.env.HOST_LISTENER||"localhost";
config.PORT = process.env.HOST_PORT||8888;


// and export all
module.exports = config;