'use strict';

const express = require('express');
const config = require('./config');

const os = require("os");
const hostname = os.hostname();
const ipinterface = os.networkInterfaces();
const hostip = (ipinterface.eth0||false)? ipinterface.eth0[0].address : '0.0.0.0';

const app = express();
const bodyParser = require('body-parser');
app.use(express.static('frontend'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const api = require('./apilib')(config,app);

// Test the stuff
app.get('/hostdata', (req,res) => {
	res.status(200).json({hostname,hostip,buildversion:process.env.buildversion});
});

app.get('/test/truefalse/', (req,res) => {
  res.status(200).json({testTrue:true,testFalse:false});
});

app.get('/test/env/', (req,res) => {
  res.status(200).json(process.env);
});

app.listen(config.PORT, config.HOST);
console.log(`Running on http://${config.HOST}:${config.PORT}`);

module.exports = app; // for testing