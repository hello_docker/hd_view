<listtitle>
<!-- <div> -->
  <div class="card">
    <ul class="list-group list-group-flush">
      <li class="list-group-item" each={ line, index in listoftitles }>{line.title} - {line.text}</li>
    </ul>
  </div>
<!-- </div> -->

<script>
  let that = this;
  this.listoftitles = [];


  superagent('get','/api/get/all').then( (res) => {
    that.listoftitles = res.body;
    that.update();
  });

</script>
</listtitle>